Welcome to my example code,
this code is meant to give a indication and by all means do not demonstrate my full capabillities!

To run this code, execute this in a terminal:
```bash
# create a clone:
git clone git@gitlab.com:cyberwaste/example-code-restfull-json.git

# change to the project directory
cd example-code-restfull-json/

# install dependecies:
npm install

# run the dev server:
npm run dev
```

this will start up using nodemon so code automagicly refreshes on change!
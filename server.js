const express = require('express')
const _ = require('lodash')
const bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

const data = require('./data/dummy_data.json')

/**
 * Nice and handy overview to help development:
 */
app.get('/', function (req, res) {

    let endpoints = {
        'get': [
            '/tickets',
            '/ticket/:id'
        ],
        'post': [
            '/ticket'
        ]
    }
    res.json(endpoints)
})

/**
 * Returns the whole list of tickets:
 */
app.get('/tickets', (request, response) => {
    response.json(data)
})

/**
 * Returns one ticket, by id.
 */
app.get('/ticket/:id', (request, response) => {

    let ticketId = Number(request.params.id)
    let ticket = _.find(data, { 'id': ticketId })

    if(ticket) {
        response.json(ticket)
    } else {
        response.status(404).send(`Cannot find ticket with id: ${ticketId}`)
    }
})

/**
 * Adds a new ticket, please note that this is NOT persisted
 * in the JSON file!
 */
app.post('/ticket', (request, response) => {
    let newTicket = request.body
    let lastTicket = _.last(data)
    let newId = lastTicket.id + 1

    newTicket.id = newId
    data.push(newTicket)

    response.json(newTicket)
})

/**
 * Update a ticket, save and return it.
 */
app.patch('/ticket/:id', (request, response) => {
    let ticketId = Number(request.params.id)
    let currentTicket = _.find(data, { 'id': ticketId })
    let currentTicketIndex = _.findIndex(data, { 'id': ticketId })
    let updatedTicket = request.body

    // updatedTicket needs to come last otherwise changes wont be overwritten:
    let updated = _.assignIn({}, currentTicket, updatedTicket)

    console.log(JSON.stringify(updated))

    data[currentTicketIndex] = updated

    response.json(updated)
})

/**
 * Deletes the ticket by id, this is NOT persisted to the JSON document!
 */
app.delete('/ticket/:id', (request, response) => {
    let ticketId = Number(request.params.id)
    let ticketIndex = _.findIndex(data, { 'id': ticketId })

    if (data[ticketIndex]) {
        delete data[ticketIndex]
        response.json({'delete': 'deleted', 'id': ticketId})
    } else {
        response.status(404).send(`Cannot delete, ticket with index ${ticketIndex} was not found!`)
    }
})



app.listen(3000, function () {
    console.log('Scrum server running on port 3000!')
})